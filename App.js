import { Flex } from "@chakra-ui/react";
import EmployeeScreen from "./EmployeeScreen";

const App = () => {
  return (
    <>
      <Flex>
        <EmployeeScreen />
      </Flex>
    </>
  );
};

export default App;
