import {
  Button,
  Flex,
  Heading,
  Text,
  FormControl,
  FormLabel,
  Input,
  Link,
  Spacer,
} from "@chakra-ui/react";
import { useState } from "react";
import FormContainer from "./FormContainer";
const EmployeeScreen = () => {
  const [name, setName] = useState("");
  const [designation, setDesignation] = useState("");
  const [contactDetails, setContactDetails] = useState("");
  const [skills, setSkills] = useState("");
  const [dob, setDob] = useState("");

  const handle = () => {
    localStorage.setItem("name", name);
    localStorage.setItem("designation", designation);
    localStorage.setItem("contactDetails", contactDetails);
    localStorage.setItem("skills", skills);
    localStorage.setItem("dob", dob);
  };

  return (
    <>
      <Flex direction="column" alignItems="center">
        <Flex
          w="full"
          alignItems="center"
          justifyContent="center"
          py="5"
          direction="column"
        >
          <FormContainer>
            <Heading as="h1" mb="8" fontSize="3xl">
              Employee Data
            </Heading>

            <form>
              <FormControl isRequired>
                <FormLabel>Name</FormLabel>
                <Input
                  type="text"
                  placeholder="Your full name"
                  // value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </FormControl>

              <Spacer h="3" />

              <FormControl isRequired>
                <FormLabel>Designation</FormLabel>
                <Input
                  type="text"
                  placeholder="Your Full Name"
                  // value={designation}
                  onChange={(e) => setDesignation(e.target.value)}
                />
              </FormControl>

              <Spacer h="3" />

              <FormControl>
                <FormLabel>Contact Details</FormLabel>
                <Input
                  type="number"
                  placeholder="Your Phone Number"
                  // value={contactDetails}
                  onChange={(e) => setContactDetails(e.target.value)}
                />
              </FormControl>

              <Spacer h="3" />

              <FormControl>
                <FormLabel>Skills</FormLabel>
                <Input
                  type="text"
                  placeholder="Your Skills"
                  // value={skills}
                  onChange={(e) => setSkills(e.target.value)}
                />
              </FormControl>

              <Spacer h="3" />

              <FormControl>
                <FormLabel>Date Of Birth</FormLabel>
                <Input
                  type="text"
                  placeholder="Your Date Of Birth"
                  // value={dob}
                  onChange={(e) => setDob(e.target.value)}
                />
              </FormControl>
              <Spacer h="3" />
            </form>
            <Button mb="2" onClick={handle}>
              Submit
            </Button>
            <Button>Delete Employee</Button>
          </FormContainer>

          <Spacer h="3" />
        </Flex>

        <Flex>
          <Button>Add Employee</Button>
        </Flex>
      </Flex>
      <Flex display="column" fontSize="xl">
        <Heading>Name: {localStorage.getItem("name")} </Heading>
        <Heading>Designation: {localStorage.getItem("designation")} </Heading>
        <Heading>Contact: {localStorage.getItem("contactDetails")} </Heading>
        <Heading>Skills: {localStorage.getItem("skills")} </Heading>
        <Heading>Date Of Birth: {localStorage.getItem("dob")} </Heading>
      </Flex>
    </>
  );
};

export default EmployeeScreen;
